# Nextcloud Docker config

Nextcloud configuration with Docker.

## License

<p xmlns:dct="http://purl.org/dc/terms/" xmlns:cc="http://creativecommons.org/ns#" class="license-text"><a rel="cc:attributionURL" href="https://gitlab.com/Emplis/nextcloud-docker-config"><span rel="dct:title">Nextcloud Docker config</span></a> by <a rel="cc:attributionURL" href="https://theobattrel.fr"><span rel="cc:attributionName">Théo Battrel</span></a> CC0 1.0<a href="https://creativecommons.org/publicdomain/zero/1.0">
<br>
<img style="height:22px!important;margin-left: 3px;vertical-align:text-bottom;" src="https://search.creativecommons.org/static/img/cc_icon.svg" /><img style="height:22px!important;margin-left: 3px;vertical-align:text-bottom;" src="https://search.creativecommons.org/static/img/cc-cc0_icon.svg" /></a></p>